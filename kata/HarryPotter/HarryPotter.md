# Feature: Harry Potter Book Shop

## Narrative:
**As a** a customer 
**I want** to determine a price of the any combination of the 5 different Harry Potter books
**So that** I could calculate the biggest discount together with the smallest price

## Scenario_1:

**Given**: There is an empty shopping cart
**When**: There is no books in the shopping cart
**Then**: The price is 0 and there is no discount

## Scenario_2:

**Given**: There is a one book in the shopping cart
**When**: There is a one book in the shopping cart
**Then**: The price is 800 cents and there is no discount

## Scenario_3:

**Given**: There is a multiple copies of one book in the shopping cart
**When**: There is a multiple copies of one book in the shopping cart
**Then**: The price is number of copies * 800 cents and there is no discount

## Scenario_4:

**Given**: There is two different books in the shopping cart
**When**: There is two different books in the shopping cart
**Then**: The price is number of copies * 800 cents and there is 5% discount

## Scenario_5:

**Given**: There is three different books in the shopping cart
**When**: There is three different books in the shopping cart
**Then**: The price is number of copies * 800 cents and there is 10% discount

## Scenario_6:

**Given**: There is four different books in the shopping cart
**When**: There is four different books in the shopping cart
**Then**: The price is number of copies * 800 cents and there is 20% discount

## Scenario_7:

**Given**: There is five different books in the shopping cart
**When**: There is five different books in the shopping cart
**Then**: The price is number of copies * 800 cents and there is 25% discount

## Scenario_8:

**Given**: There is multiple books in the shopping cart
**When**: There is possibility of 25% and 10% discount and two times 20% discount
**Then**: The lowest price is when two times 20% discount is applied