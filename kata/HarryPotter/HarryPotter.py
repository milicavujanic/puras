"""Harry Potter modul"""
from enum import Enum

SINGLE_BOOK_PRICE = 800
discount_dictionary = {1:1,2:0.95,3:0.9,4:0.8,5:0.75}

class HarryPotterBooks (Enum):
  HarryPotterAndThePhilosopherStone = 1
  HarryPotterAndTheChamberOfSecrets = 2
  HarryPotterAndThePrisonerFromAskaban = 3
  HarryPotterAndTheGobletOfFire = 4
  HarryPotterAndTheOrderOfPhoenix = 5

class HarryPotterShoppingCart:
    """Harry Potter Shopping Cart class"""

    def __init__(self):
      self.books = {}

    def getPriceInCents(self):
      remaining_books = dict(self.books)
      number_of_remaining_books = sum (remaining_books.values())
      total = 0
      temp_list = []
      while (number_of_remaining_books > 0):
        number_of_distinct_books = len(remaining_books)
        temp_list.append(number_of_distinct_books)
        self.removeOneIssueOfEachBook(remaining_books)
        number_of_remaining_books = len (remaining_books)
      counter_of_fives = 0
      counter_of_threes = 0 
      for elem in temp_list:
        if elem == 5:
          counter_of_fives +=1
        elif elem == 3:
          counter_of_threes +=1
        else:
          total += elem * SINGLE_BOOK_PRICE * discount_dictionary.get(elem)
      if (counter_of_fives > 0 and counter_of_threes >0):
       if counter_of_fives == counter_of_threes:
         total += 4 * (counter_of_fives + counter_of_threes) * SINGLE_BOOK_PRICE * discount_dictionary.get(4)
       elif counter_of_fives > counter_of_threes:
         total += 4 * counter_of_fives * SINGLE_BOOK_PRICE * discount_dictionary.get(4)
         total += 5 * counter_of_threes * SINGLE_BOOK_PRICE * discount_dictionary.get(5)
       else:
         total += 4 * counter_of_threes * SINGLE_BOOK_PRICE * discount_dictionary.get(4)
         total += 3 * counter_of_fives * SINGLE_BOOK_PRICE * discount_dictionary.get(3)
      else:
        total += 3 * counter_of_threes * SINGLE_BOOK_PRICE * discount_dictionary.get(3)
        total += 5 * counter_of_fives * SINGLE_BOOK_PRICE * discount_dictionary.get(5)
      return total

    def removeOneIssueOfEachBook (self,remaining_books):
        distinct_books = set (remaining_books.keys())
        for key in distinct_books:
          amount = remaining_books.get(key)
          if (amount == 1):
            remaining_books.pop(key)
          else:
            remaining_books[key] = amount-1
      
    def addBook(self,book):
      amount = 0
      if book in self.books:
        amount = self.books.get(book)
      self.books[book] = amount+1
