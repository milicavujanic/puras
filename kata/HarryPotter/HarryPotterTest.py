"""Test modul for HarryPotterShoppingCart class"""

import unittest

from HarryPotter import HarryPotterShoppingCart
from HarryPotter import HarryPotterBooks


class TestHarryPotter(unittest.TestCase):
    """Test class for HarryPotterShoppingCart class"""

    def setUp(self):
        self.order = HarryPotterShoppingCart()

    def calculatePrice(self,value):
        priceInCents = self.order.getPriceInCents()
        self.assertEqual(priceInCents, value)

    def test_should_return_zero_cents_for_empty_cart(self):
        self.calculatePrice(0)
        
    def test_should_return_800_cents_for_first_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.calculatePrice(800)
    
    def test_should_return_800_cents_for_second_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.calculatePrice(800)
    
    def test_should_return_800_cents_for_third_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.calculatePrice(800)

    def test_should_return_800_cents_for_fourth_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.calculatePrice(800)

    def test_should_return_800_cents_for_fifth_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(800)
    
    def test_should_return_1600_cents_for_two_copies_of_fourth_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.calculatePrice(1600)
    
    def test_should_return_price_with_five_percent_discount_for_two_different_books(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.calculatePrice(1520)

    def test_should_return_price_with_five_percent_discount_for_two_pair_of_two_different_books(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.calculatePrice(3040)
    
    def test_should_return_price_of_two_same_and_one_different_book(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(2320)

    def test_should_return_ten_percent_discount_for_three_different_books(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.calculatePrice(2160)
    
    def test_should_return_ten_percent_discount_for_four_books_one_duplicate(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.calculatePrice(2960)
    
    def test_should_return_twenty_percent_discount_for_four_different_books(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.calculatePrice(2560)
    
    def test_should_return_twenty_five_percent_discount_for_five_different_books(self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(3000)
    
    def test_should_return_two_times_twenty_percent_discount (self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(5120)

    def test_should_return_two_times_twenty_and_one_time_twenty_five_percent_discount (self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(8120)
    
    def test_should_return_two_times_twenty_and_one_time_ten_percent_discount (self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(7280)

    def test_should_return_four_times_twenty_percent_discount (self):
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePhilosopherStone)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheChamberOfSecrets)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndThePrisonerFromAskaban)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheGobletOfFire)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.order.addBook(HarryPotterBooks.HarryPotterAndTheOrderOfPhoenix)
        self.calculatePrice(10240)


unittest.main()
